#!/bin/bash
CMAKE_STR=$(echo $(cmake --version | grep version))
echo "Cmake version: '$CMAKE_STR'"
if [[ $CMAKE_STR = *"version"* ]]; then
    CMAKE_VERSION=$(echo $CMAKE_STR | awk -F' ' '{print $3}')
    REQUIRED_VERSION=7
    CMAKE_MINOR=$(echo $CMAKE_VERSION | awk -F. '{print $2}')
    echo "$CMAKE_MINOR"
    if [[ $CMAKE_MINOR -lt $REQUIRED_VERSION ]]; then
        echo "Please install a compatible version of cmake (3.7.1 or higher)"
        exit
    fi
fi
sudo apt-get install libcurl4-openssl-dev
sudo apt-get install libboost-dev

sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential \
     chrpath socat libsdl1.2-dev xterm make xsltproc docbook-utils fop dblatex xmlto autoconf \
     automake libtool libglib2.0-dev libarchive-dev python-git
