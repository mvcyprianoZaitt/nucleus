#include "IOManager.h"

#include <QJsonObject>

#include <string>
#include <map>
#include <memory>
#include <mutex>

#include "defines.h"
#include "file_system/JSonFile.h"
#include "FileManager.h"
#include "wiringPi.h"
#include "softPwm.h"
#include "spdlog/spdlog.h"

IOManager::IOManager(std::shared_ptr<spdlog::logger> logger, IOPins pins,
                     VarStatus snapshot_vars)
        : logger_(logger),
          pins_(pins) {
    logger_->info("IOManager: Initializing io manager.");
    wiringPiSetupPhys();
    SetUp();
    SetPinsInitValues(snapshot_vars);
}

IOManager::~IOManager() { }
void IOManager::Write(OutputPins pin, bool value) const {
    try {
        int pin_channel = static_cast<int>(pins_.output_pins_.at(pin));
        digitalWrite(pin_channel, value);
    } catch (std::out_of_range &ex) {
        logger_->error("IOManager: Tried to write an invalid pin {0:d}",
                       static_cast<int>(pin));
    }
}

bool IOManager::Read(OutputPins pin) const {
    // read specific pin from GPIO
    try {
        int pin_channel = static_cast<int>(pins_.output_pins_.at(pin));
        return digitalRead(pin_channel);
    } catch (std::out_of_range &ex) {
        logger_->error("IOManager: Tried to read an invalid pin {0:d}",
                       static_cast<int>(pin));
    }
}

bool IOManager::Read(InputPins pin) const {
    // read specific pin from GPIO
    try {
        int pin_channel = static_cast<int>(pins_.input_pins_.at(pin));
        return digitalRead(pin_channel);
    } catch (std::out_of_range &ex) {
        logger_->error("IOManager: Tried to read an invalid pin {0:d}",
                       static_cast<int>(pin));
    }
}

void IOManager::WritePwm(OutputPins pin, int32_t value) {
    try {
        int pin_channel = static_cast<int>(pins_.output_pins_.at(pin));
        softPwmWrite(pin_channel, value);
    } catch (std::out_of_range &ex) {
        logger_->error("IOManager: Tried to write an invalid pin {0:d}",
                       static_cast<int>(pin));
    }
}


void IOManager::SetUp() {
    logger_->info("IOManager: Set up wiring pi pins.");
    for (auto &it : pins_.input_pins_) {
        pinMode(it.second, INPUT);
        if (it.first == InputPins::kSmokeDetector){
            pullUpDnControl(it.second, PUD_DOWN);
        } else {
            pullUpDnControl(it.second, PUD_UP);
        }
    }
    for (auto &it : pins_.output_pins_) {
        if (it.first == OutputPins::kLightFacadeChannelR ||
            it.first == OutputPins::kLightFacadeChannelG ||
            it.first == OutputPins::kLightFacadeChannelB){
            softPwmCreate(it.second, 0, 100);
        } else{
            pinMode(it.second, OUTPUT);
        }
    }
}

void IOManager::SetPinsInitValues(VarStatus snapshot_vars){
    logger_->info("IOManager: Initializing pins.");

    if (!snapshot_vars.snapshot_valid){
        for (auto &it : pins_.output_pins_) {
            if (it.first == OutputPins::kFrontDoor)
                Write(OutputPins::kFrontDoor, false);
            else if (it.first == OutputPins::kExitDoor)
                Write(OutputPins::kExitDoor, false);
            else if (it.first == OutputPins::kACDevice)
                Write(OutputPins::kACDevice, true);
            else if (it.first == OutputPins::kACDeviceAux)
                Write(OutputPins::kACDeviceAux, true);
            else if (it.first == OutputPins::kLightCircuit)
                Write(OutputPins::kLightCircuit, true);
            else if (it.first == OutputPins::kLightFacadePower)
                Write(OutputPins::kLightFacadePower, true);
            else if (it.first == OutputPins::kLightFacadeChannelR)
                WritePwm(OutputPins::kLightFacadeChannelR, 80);
            else if (it.first == OutputPins::kLightFacadeChannelG)
                WritePwm(OutputPins::kLightFacadeChannelG, 80);
            else if (it.first == OutputPins::kLightFacadeChannelB)
                WritePwm(OutputPins::kLightFacadeChannelB, 80);
        }
    } else{
        for (auto &it : pins_.output_pins_) {
            if (it.first == OutputPins::kFrontDoor)
                Write(OutputPins::kFrontDoor, snapshot_vars.front_door_open);
            else if (it.first == OutputPins::kExitDoor)
                Write(OutputPins::kExitDoor, snapshot_vars.exit_door_open);
            else if (it.first == OutputPins::kACDevice)
                Write(OutputPins::kACDevice, snapshot_vars.ac_main_device);
            else if (it.first == OutputPins::kACDeviceAux)
                Write(OutputPins::kACDeviceAux, snapshot_vars.ac_aux_device);
            else if (it.first == OutputPins::kLightCircuit)
                Write(OutputPins::kLightCircuit, true);
            else if (it.first == OutputPins::kLightFacadePower)
                Write(OutputPins::kLightFacadePower,
                      snapshot_vars.ligth_front_rgb);
            else if (it.first == OutputPins::kLightFacadeChannelR)
                WritePwm(OutputPins::kLightFacadeChannelR, 80);
            else if (it.first == OutputPins::kLightFacadeChannelG)
                WritePwm(OutputPins::kLightFacadeChannelG, 80);
            else if (it.first == OutputPins::kLightFacadeChannelB)
                WritePwm(OutputPins::kLightFacadeChannelB, 80);
        }
    }
}
