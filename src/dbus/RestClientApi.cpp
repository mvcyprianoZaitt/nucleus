#include "RestClientApi.h"

#include <QString>
#include <QDBusReply>
#include <QDBusConnection>

#include "RestClientInterface.h"

namespace nucleus {
namespace dbus {

RestClientApi::RestClientApi(std::shared_ptr<spdlog::logger> logger)
        : logger_(logger) {
    logger_->info("RestClientApi: starting rest client dbus interface");
    rest_client_ = new local::RestClient("org.natasha.restapi", "/RestClient",
            QDBusConnection::systemBus(), 0);
}

RestClientApi::~RestClientApi() { }

void RestClientApi::PostRequest(const QString &data, const QString &endpoint) {
    logger_->info("Received a data to send via dbus interface: {}",
                  data.toStdString());
    rest_client_->Post(data, endpoint);
}

}
}
