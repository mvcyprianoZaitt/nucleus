#ifndef AUDIOCLIENT_H
#define AUDIOCLIENT_H

#include <QObject>
#include <QDBusInterface>
#include <string>
#include <QDBusReply>
#include <QDebug>

#include "spdlog/spdlog.h"




namespace nucleus {
namespace dbus {

class AudioClient : public QObject{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Client", "org.nucleus")
public:
    AudioClient(std::shared_ptr<spdlog::logger> logger);
    virtual ~AudioClient();
    void CallAudioPlayer(const std::string audio_text);
private:
    QDBusInterface* audio_interface_;
    std::shared_ptr<spdlog::logger> logger_;


};
}
}

#endif
