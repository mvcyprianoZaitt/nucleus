#include "AudioClient.h"


namespace nucleus {
namespace dbus {

AudioClient::AudioClient(std::shared_ptr<spdlog::logger> logger)
            : logger_(logger){
    if (logger_) {
        logger_->info("AudioClient: Starting audio client.");
    }
    audio_interface_ = new QDBusInterface("org.nucleus", "/audioPlayer",
            "org.nucleus.PlayAudio", QDBusConnection::systemBus());
}

AudioClient::~AudioClient(){
}


void AudioClient::CallAudioPlayer(const std::string audio_text){
    QDBusReply<int> reply = audio_interface_->call("tts_", audio_text.c_str());
    logger_->info("AudioClient: Sending audio through D-Bus interface. Audio: {}",
            audio_text);
}

}
}
