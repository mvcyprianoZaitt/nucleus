#ifndef RESTCLIENTAPI_H
#define RESTCLIENTAPI_H

#include <QString>

#include <memory>

#include "RestClientInterface.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace dbus {

class RestClientApi {

 public:
     RestClientApi(std::shared_ptr<spdlog::logger> logger);
     ~RestClientApi();
     /**
     * Consume DBus method to create a post request.
     */
     void PostRequest(const QString &data, const QString &endpoint);
     std::shared_ptr<spdlog::logger> logger_;
     local::RestClient *rest_client_;
};
}
}

#endif
