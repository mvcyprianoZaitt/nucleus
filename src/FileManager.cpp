#include "FileManager.h"

#include <memory>
#include <map>
#include <QDate>
#include <QDateTime>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QTime>
#include <string>

#include "defines.h"
#include "monitoring/SnapshotManager.h"
#include "spdlog/spdlog.h"


char* FileManager::kJsonKeyOutputPins ="output_pins";
char* FileManager::kJsonKeyInputPins = "input_pins";
using nucleus::file_system::JSonFile;
FileManager::FileManager(std::shared_ptr<spdlog::logger> logger)
        : json_file_(CONFIGURATION_FILE, logger),
          snapshot_file_(SNAPSHOT_FILE, logger),
          logger_(logger) {
	  logger_->info("FileManager: Starting file manager.");
    ParsePinsJson();
    ParseSnapshot();
    ParseJson();
}

FileManager::~FileManager() {

}

void FileManager::ParseJson() {
    QJsonObject object = json_file_.GetObject();
    if (object.isEmpty()) {
        logger_->error("FileManager: JSon object empty.");
        return;
    }
    if (object.contains("iot_config")) {
        QJsonObject iot_config = object.take("iot_config").toObject();
        if (iot_config.contains("key"))
            pusher_config_.key = iot_config.take("key").toString().toStdString();
        if (iot_config.contains("channel"))
            pusher_config_.channel = iot_config.take("channel").toString()
                    .toStdString();
        if (iot_config.contains("cluster"))
            pusher_config_.cluster = iot_config.take("cluster").toString()
                    .toStdString();
    }
    logger_->info("FileManager: pusher -> key {} || channel: {} || cluster: {}",
                  pusher_config_.key, pusher_config_.channel,
                  pusher_config_.cluster);
}

void FileManager::ParseSnapshot() {
    logger_->info("FileManager: Loading snapshot");
    QJsonObject snapshot_object = snapshot_file_.GetObject();
    if (snapshot_object.isEmpty()) {
        logger_->error("FileManager: Snapshot file is empty.");
        return;
    }
    if (snapshot_object.contains("last_snapshot")) {
        QJsonObject last_snapshot = snapshot_object.take(
                                    "last_snapshot").toObject();
        if (last_snapshot.contains("date") && last_snapshot.contains("time")){
            QDateTime snaphot_date_time = QDateTime::fromString(
                    last_snapshot.take("date").toString()+
                    " "+last_snapshot.take("time").toString(),
                    "dd.MM.yyyy hh:mm:ss");
            QDateTime now = QDateTime::currentDateTime();
            if (snaphot_date_time.secsTo(now) < 24*60*60){
                logger_->info("FileManager: Snapshot file is valid.");
                if (snapshot_object.contains("internal_state")) {
                    snapshot_vars_.snapshot_valid = true;
                    QJsonObject internal_state = snapshot_object.take(
                          "internal_state").toObject();
                    if (internal_state.contains("front_door_open"))
                        snapshot_vars_.front_door_open = internal_state.take(
                          "front_door_open").toBool();
                    if (internal_state.contains("exit_door_open"))
                        snapshot_vars_.exit_door_open = internal_state.take(
                          "exit_door_open").toBool();
                    if (internal_state.contains("ligth_front_rgb"))
                        snapshot_vars_.ligth_front_rgb = internal_state.take(
                          "ligth_front_rgb").toBool();
                    if (internal_state.contains("ac_main_device"))
                        snapshot_vars_.ac_main_device = internal_state.take(
                          "ac_main_device").toBool();
                    if (internal_state.contains("ac_aux_device"))
                        snapshot_vars_.ac_aux_device = internal_state.take(
                          "ac_aux_device").toBool();
                    if (internal_state.contains("store_open"))
                        snapshot_vars_.store_open = internal_state.take(
                          "store_open").toBool();
                }
            } else {
                logger_->error("FileManager: Snapshot file is not valid.");
            }
        }
    }
}

IoTConfig FileManager::GetIoTConfig() {
    return pusher_config_;
}

void FileManager::ParsePinsJson() {
    QJsonObject json_object = json_file_.GetObject();
    QJsonObject output_object = json_object.take(kJsonKeyOutputPins)
            .toObject();
    ParseJsonOutput(output_object);

    QJsonObject input_object = json_object.take(kJsonKeyInputPins)
            .toObject();
    ParseJsonInput(input_object);
}

void FileManager::ParseJsonOutput(QJsonObject output_object) {
    logger_->info("FileManager: configuring Output pins");
    int frontdoor = 3;
    int exit_door = 5;
    int ac_device = 19;
    int ac_device_aux = 11;
    int lighting_circuit = 13;
    int lighting_facade_power = 33;
    int lighting_facade_r = 15;
    int lighting_facade_g = 16;
    int lighting_facade_b = 18;
    if (output_object.contains("frontdoor"))
        frontdoor = output_object.take("frontdoor").toInt();
    if (output_object.contains("exit_door"))
        exit_door = output_object.take("exit_door").toInt();
    if (output_object.contains("ac_device"))
        ac_device = output_object.take("ac_device").toInt();
    if (output_object.contains("ac_device_aux"))
        ac_device_aux = output_object.take("ac_device_aux").toInt();
    if (output_object.contains("lighting_circuit"))
        lighting_circuit = output_object.take("lighting_circuit").toInt();
    if (output_object.contains("lighting_facade_power"))
        lighting_facade_power = output_object.take("lighting_facade_power")
            .toInt();
    if (output_object.contains("lighting_facade_r"))
        lighting_facade_r = output_object.take("lighting_facade_r").toInt();
    if (output_object.contains("lighting_facade_g"))
        lighting_facade_g = output_object.take("lighting_facade_g").toInt();
    if (output_object.contains("lighting_facade_b"))
        lighting_facade_b = output_object.take("lighting_facade_b").toInt();

    logger_->info("FileManager: Output front door pin -> {0:d}", frontdoor);
    pins_.output_pins_[OutputPins::kFrontDoor] = frontdoor;
    logger_->info("FileManager: Output exit door pin -> {0:d}", exit_door);
    pins_.output_pins_[OutputPins::kExitDoor] = exit_door;
    logger_->info("FileManager: Output ac device pin -> {0:d}", ac_device);
    pins_.output_pins_[OutputPins::kACDevice] = ac_device;
    logger_->info("FileManager: Output auxiliary ac pin -> {0:d}",
                   ac_device_aux);
    pins_.output_pins_[OutputPins::kACDeviceAux] = ac_device_aux;
    logger_->info("FileManager: Output lighting circuit pin -> {0:d}",
                   lighting_circuit);
    pins_.output_pins_[OutputPins::kLightCircuit] = lighting_circuit;
    logger_->info("FileManager: Output lighting facade power pin -> {0:d}",
                   lighting_facade_power);
    pins_.output_pins_[OutputPins::kLightFacadePower] = lighting_facade_power;
    logger_->info("FileManager: Output lighting facade R pin -> {0:d}",
                   lighting_facade_r);
    pins_.output_pins_[OutputPins::kLightFacadeChannelR] = lighting_facade_r;
    logger_->info("FileManager: Output lighting facade G pin -> {0:d}",
                   lighting_facade_g);
    pins_.output_pins_[OutputPins::kLightFacadeChannelG] = lighting_facade_g;
    logger_->info("FileManager: Output lighting facade B pin -> {0:d}",
                   lighting_facade_b);
    pins_.output_pins_[OutputPins::kLightFacadeChannelB] = lighting_facade_b;
    logger_->info("FileManager: Output pins were defined.");
}

void FileManager::ParseJsonInput(QJsonObject input_object) {
    logger_->info("FileManager: configuring Input pins");
    int frontdoor = 10;
    int exit_door = 8;
    int smoke_detector = 22;
    int temperature_sensor = 7;
    int exit_door_emergency = 12;
    if (input_object.contains("frontdoor")) {
        frontdoor = input_object.take("frontdoor").toInt();
    }
    if (input_object.contains("exit_door"))
        exit_door = input_object.take("exit_door").toInt();
    if (input_object.contains("smoke_detector"))
        smoke_detector = input_object.take("smoke_detector").toInt();
    if (input_object.contains("temperature_sensor"))
        temperature_sensor = input_object.take("temperature_sensor").toInt();
    if (input_object.contains("exit_door_emergency"))
        exit_door_emergency = input_object.take("exit_door_emergency").toInt();

    logger_->info("FileManager: Input front door pin -> {0:d}", frontdoor);
    pins_.input_pins_[InputPins::kFrontDoor] = frontdoor;
    logger_->info("FileManager: Input exit door pin -> {0:d}", exit_door);
    pins_.input_pins_[InputPins::kExitDoor] = exit_door;
    logger_->info("FileManager: Input smoke detector pin -> {0:d}",
                   smoke_detector);
    pins_.input_pins_[InputPins::kSmokeDetector] = smoke_detector;
    logger_->info("FileManager: Input temperature sensor pin -> {0:d}",
                   temperature_sensor);
    pins_.input_pins_[InputPins::kTemperatureSensor] = temperature_sensor;
    logger_->info("FileManager: Input exit emergency pin -> {0:d}",
                   exit_door_emergency);
    pins_.input_pins_[InputPins::kExitDoorEmergency] = exit_door_emergency;
    logger_->info("FileManager: Input pins were defined.");
}

IOPins FileManager::GetIOPinsStruct(){
    return pins_;
}

VarStatus FileManager::GetSnapshotStruct(){
    return snapshot_vars_;
}
