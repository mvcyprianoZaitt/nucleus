#ifndef MAINENGINE_H
#define MAINENGINE_H

#include <QObject>
#include <QTimer>
#include "iot_client/PusherClient.h"
#include "monitoring/Debouncer.h"
#include "monitoring/SnapshotManager.h"
#include "monitoring/MonitoringFacade.h"

class MainEngine : public QObject {
    Q_OBJECT
 public:
    MainEngine(std::shared_ptr<nucleus::iot_client::PusherClient> pusher_client,
            std::shared_ptr<nucleus::monitoring::Debouncer> exit_button,
      std::shared_ptr<nucleus::monitoring::SnapshotManager> snapshot_manager);
    virtual ~MainEngine();
 private:
    void Run();
    std::shared_ptr<nucleus::monitoring::SnapshotManager> snapshot_manager_;
    std::shared_ptr<nucleus::monitoring::Debouncer> exit_button_;
    std::shared_ptr<nucleus::iot_client::PusherClient> pusher_client_;
    QTimer *run_timer_;
};

#endif
