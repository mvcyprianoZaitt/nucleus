#ifndef JSonFile_H
#define JSonFile_H
#include <QString>
#include <QFile>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QDebug>
#include <QFileInfo>


#include "spdlog/spdlog.h"

namespace nucleus {
namespace file_system {
class JSonFile {
    public:
        JSonFile(QString path, std::shared_ptr<spdlog::logger> logger);
        virtual ~JSonFile();
        void WriteFileJson(QString file_name);
        QJsonObject GetObject();
    private:
        void ReadFileJson();
        QJsonObject json_object_;
        QString read_path_;
        std::shared_ptr<spdlog::logger> logger_;
};

}
}

#endif
