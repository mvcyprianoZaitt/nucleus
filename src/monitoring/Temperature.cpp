#include "Temperature.h"

#include <QDir>
#include <QString>
#include <QStringList>

#include "DS18B20.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace monitoring {

Temperature::Temperature(std::shared_ptr<spdlog::logger> logger)
        : logger_(logger){

    logger_->info("Temperature: starting temperature sensor");

    QDir device_directory("/sys/bus/w1/devices/");
    if (!device_directory.exists()){
        w1Device_ = nullptr;
        logger_->error("Temperature: Device directory not found");
        return;
    }

    QStringList files_in_dir = device_directory.entryList();
    if (files_in_dir.isEmpty()){
        w1Device_ = nullptr;
        logger_->error("Temperature: DS18B20 sensor not found");
        return;
    }

    foreach(QString filename, files_in_dir) {
        if (filename.indexOf("28") == 0){
            w1Device_ = std::make_shared<DS18B20>(filename.toLatin1().data());
            logger_->info("Temperature: Device found");
            return;
        }
    }
    w1Device_ = nullptr;
    logger_->error("Temperature: DS20B18 sensor not found");
}

Temperature::Temperature(const char* address,
        std::shared_ptr<spdlog::logger> logger)
        : logger_(logger){

    logger_->info("Temperature: starting temperature sensor");
    w1Device_ = std::make_shared<DS18B20>(address);
}

Temperature::~Temperature(){
}

double Temperature::GetValue(){
    if (w1Device_ != nullptr){
        logger_->info("Temperature: Getting temperature value");
        return double(w1Device_->getTemp());
    } else {
        logger_->error("Temperature: Cannot get temperature");
        return -1.0;
    }

}

}
}
