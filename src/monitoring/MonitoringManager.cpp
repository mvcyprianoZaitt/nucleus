#include "MonitoringManager.h"


#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QTimer>
#include <QString>

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <utility>

#include "spdlog/spdlog.h"
#include "src/dbus/AudioClient.h"
#include "src/iot_client/PusherClient.h"
#include "src/dbus/RestClientApi.h"
#include "src/IOManager.h"
#include "Temperature.h"

using nucleus::iot_client::PusherClient;

namespace nucleus {
namespace monitoring {
const std::map<PostIotEvents, std::string> MonitoringManager::kPostRoutes = {
    {PostIotEvents::kDoorTimeout, ""},
    {PostIotEvents::kSmokeDetector, ""},
    {PostIotEvents::kTempResponse, "temperature"},
    {PostIotEvents::kACDeviceAux, "status"},
    {PostIotEvents::kLightCircuitStatus, "status"}
};
MonitoringManager::MonitoringManager(std::shared_ptr<spdlog::logger> logger,
       std::shared_ptr<nucleus::iot_client::PusherClient> pusher_client,
       std::shared_ptr<IOManager> io_manager,
       std::shared_ptr<nucleus::dbus::RestClientApi> rest_client,
       VarStatus snapshot_vars)
       : logger_(logger),
         pusher_client_(pusher_client),
         temperature_(logger_),
         io_manager_(io_manager),
         rest_client_(rest_client),
         audio_(logger_),
         snapshot_vars_(snapshot_vars),
         entrance_door_count_(0),
         open_store_(true),
         persist_door_open_(false),
         exit_door_count_(0) {

    if (snapshot_vars_.snapshot_valid){
        open_store_ = snapshot_vars_.store_open;
    }
    close_door_timer_ = new QTimer(this);
    close_door_timer_->setSingleShot(true);
    close_entrance_door_timer_ = new QTimer(this);
    close_entrance_door_timer_->setSingleShot(true);
    close_exit_door_timer_ = new QTimer(this);
    close_exit_door_timer_->setSingleShot(true);
    check_door_timer_ = new QTimer(this);
    connect(close_door_timer_, &QTimer::timeout, this,
           &MonitoringManager::CloseDoorCommand);
    connect(close_entrance_door_timer_, &QTimer::timeout, this,
           &MonitoringManager::CloseEntranceDoorCommand);
    connect(close_exit_door_timer_, &QTimer::timeout, this,
           &MonitoringManager::CloseExitDoorCommand);
    connect(check_door_timer_, &QTimer::timeout, this,
           &MonitoringManager::CheckDoor);
    connect(pusher_client_.get(), &PusherClient::ConnectedSignal, this,
           &MonitoringManager::SubscribeCallbacks);

    //activete opened door checked
    int time_ms = 10000;
    check_door_timer_->start(time_ms);

}

MonitoringManager::~MonitoringManager() {
    close_door_timer_->deleteLater();
    check_door_timer_->deleteLater();
}

void MonitoringManager::SubscribeCallbacks() {
    pusher_client_->SubscribeToEvent("iot_event_type_0",
            std::bind(&MonitoringManager::OpenStoreRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_1",
            std::bind(&MonitoringManager::OpenStoreRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_2",
            std::bind(&MonitoringManager::OpenDoorPermanentlyRequestEvent,
                      this, std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_3",
            std::bind(&MonitoringManager::CloseDoorRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_4",
            std::bind(&MonitoringManager::OpenDoorMomentRequestEvent, this,
                      std::placeholders::_1));
    pusher_client_->SubscribeToEvent("iot_event_type_5",
            std::bind(&MonitoringManager::AudioRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_6",
            std::bind(&MonitoringManager::OpenDoorClientRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_7",
            std::bind(&MonitoringManager::OpenEntranceDoorClientRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_8",
            std::bind(&MonitoringManager::OpenExitDoorClientRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_10",
            std::bind(&MonitoringManager::TemperatureRequestEvent, this,
                      std::placeholders::_1));
    pusher_client_->SubscribeToEvent("iot_event_type_11",
            std::bind(&MonitoringManager::SendACStatusRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_12",
            std::bind(&MonitoringManager::TurnOnACRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_13",
            std::bind(&MonitoringManager::TurnOffACRequestEvent, this,
                      std::placeholders::_1));
    pusher_client_->SubscribeToEvent("iot_event_type_14",
            std::bind(&MonitoringManager::SendLuminosityStatusRequestEvent,
                      this, std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_15",
            std::bind(&MonitoringManager::TurnOnLightsRequestEvent, this,
                      std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_16",
            std::bind(&MonitoringManager::TurnOffLightsRequestEvent, this,
                      std::placeholders::_1)) ;
    //pusher_client_->SubscribeToEvent("iot_event_type_18",
    //        std::bind(&MonitoringManager::UpdateCodeRequestEvent, this,
    //                std::placeholders::_1)) ;
    pusher_client_->SubscribeToEvent("iot_event_type_22",
            std::bind(&MonitoringManager::FrontRgb, this,
                      std::placeholders::_1));
}

void MonitoringManager::TemperatureRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Temperature request.");
    float temp = temperature_.GetValue();
    if (temp > 0){
        logger_->info("MonitoringManager: Temperature read: {0.2:f}", temp);
        QString stemp = QString::number(temp);
        PostResponse(stemp, PostIotEvents::kTempResponse);
    }
}

void MonitoringManager::OpenStoreRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received an Open Store request.");
    open_store_ = true;
}

void MonitoringManager::CloseStoreRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Close Store request.");
    open_store_ = false;
}

void MonitoringManager::OpenDoorPermanentlyRequestEvent(
        const std::string &data) {
    logger_->info("MonitoringManager: Received a Open Door Permanently request."
                  "");
    persist_door_open_ = true;
    io_manager_->Write(OutputPins::kFrontDoor, true);
    io_manager_->Write(OutputPins::kExitDoor, true);
}

void MonitoringManager::CloseDoorRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Close Door request.");
    persist_door_open_ = false;
    io_manager_->Write(OutputPins::kFrontDoor, false);
    io_manager_->Write(OutputPins::kExitDoor, false);
}

void MonitoringManager::OpenDoorMomentRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Open Door Moment request.");
    int time_ms = 5000;
    if (!persist_door_open_) {
        io_manager_->Write(OutputPins::kFrontDoor, true);
        io_manager_->Write(OutputPins::kExitDoor, true);
        if (close_door_timer_->isActive()) {
            close_door_timer_->stop();
        }
        close_door_timer_->start(time_ms);
    }
}

void MonitoringManager::AudioRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received an Audio Request Moment"
                  "request.");
    QString Qdata = QString::fromStdString(data);
    QJsonObject object;
    QJsonDocument doc = QJsonDocument::fromJson(Qdata.toUtf8());
    if(!doc.isNull()){
        if(doc.isObject()){
            object = doc.object();
            if (object.contains("metadata")) {
                QJsonObject metadata = object.take("metadata").toObject();
                if (metadata.contains("audio"))
                    audio_.CallAudioPlayer(metadata.take("audio").toString()
                                    .toStdString());//.toLatin1().data());
                }
            } else {
            logger_->info("MonitoringManager: error parsing JSON.");
        }
    } else {
        logger_->info("MonitoringManager: invalid JSON.");
    }

}

void MonitoringManager::OpenDoorClientRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Open Door Client request.");
    int time_ms = 5000;
    if (open_store_ && !persist_door_open_) {
        io_manager_->Write(OutputPins::kFrontDoor, true);
        io_manager_->Write(OutputPins::kExitDoor, true);
        if (close_door_timer_->isActive()) {
            close_door_timer_->stop();
        }
        close_door_timer_->start(time_ms);
    }
}

void MonitoringManager::OpenEntranceDoorClientRequestEvent(
                                                    const std::string &data) {
    logger_->info("MonitoringManager: Received a Open Entrance Door request.");
    int time_ms = 5000;
    if (open_store_ && !persist_door_open_) {
        io_manager_->Write(OutputPins::kFrontDoor, true);
        if (close_entrance_door_timer_->isActive()) {
            close_entrance_door_timer_->stop();
        }
        close_entrance_door_timer_->start(time_ms);
    }
}

void MonitoringManager::OpenExitDoorClientRequestEvent(
                                                    const std::string &data) {
    logger_->info("MonitoringManager: Received a Open Exit Door request.");
    int time_ms = 5000;
    if (open_store_ && !persist_door_open_) {
        io_manager_->Write(OutputPins::kExitDoor, true);
        if (close_exit_door_timer_->isActive()) {
            close_exit_door_timer_->stop();
        }
        close_exit_door_timer_->start(time_ms);
    }
}

void MonitoringManager::SendACStatusRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Send AC Status request.");
    bool status = io_manager_->Read(OutputPins::kACDeviceAux);
    QString status_text = status ? "true" : "false";
    logger_->info("MonitoringManager: AC status: {}",status_text.toStdString());
    PostResponse(status_text, PostIotEvents::kACDeviceAux);
}

void MonitoringManager::TurnOnACRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Turn on AC request.");
    io_manager_->Write(OutputPins::kACDeviceAux, true);
}

void MonitoringManager::TurnOffACRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Turn off AC request.");
    io_manager_->Write(OutputPins::kACDeviceAux, false);
}

void MonitoringManager::SendLuminosityStatusRequestEvent(
        const std::string &data) {
    logger_->info("MonitoringManager: Received a Send Luminosity Status "
                  "request.");
    bool status = io_manager_->Read(OutputPins::kLightCircuit);
    QString status_text = status ? "true" : "false";
    logger_->info("MonitoringManager: Luminosity status: {}"
            ,status_text.toStdString());
    PostResponse(status_text, PostIotEvents::kLightCircuitStatus);
}

void MonitoringManager::TurnOnLightsRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Turn on Lights request.");
    io_manager_->Write(OutputPins::kLightCircuit, true);
}

void MonitoringManager::TurnOffLightsRequestEvent(const std::string &data) {
    logger_->info("MonitoringManager: Received a Turn off Lights request.");
    io_manager_->Write(OutputPins::kLightCircuit, true);
}

void MonitoringManager::CloseDoorCommand() {
    io_manager_->Write(OutputPins::kFrontDoor, false);
    io_manager_->Write(OutputPins::kExitDoor, false);
    logger_->info("MonitoringManager: Closing doors");
}

void MonitoringManager::CheckDoor(){
    if (io_manager_->Read(InputPins::kFrontDoor)){
        entrance_door_count_++;
    } else {
        entrance_door_count_ = 0;
    }
    if (entrance_door_count_ > 6){
        entrance_door_count_ = 0;
        PostResponse("", PostIotEvents::kDoorTimeout);
        logger_->info("MonitoringManager: Store entrance door opened for 1 minute");
    }

    if (io_manager_->Read(InputPins::kExitDoor)){
        exit_door_count_++;
    } else {
        exit_door_count_ = 0;
    }
    if (exit_door_count_ > 6){
        exit_door_count_ = 0;
        PostResponse("", PostIotEvents::kDoorTimeout);
        logger_->info("MonitoringManager: Store exit door opened for 1 minute");
    }
}

void MonitoringManager::CloseEntranceDoorCommand() {
    io_manager_->Write(OutputPins::kFrontDoor, false);
    logger_->info("MonitoringManager: Closing entrance door");
    //close_entrance_door_timer_->stop();
}

void MonitoringManager::CloseExitDoorCommand() {
    io_manager_->Write(OutputPins::kExitDoor, false);
    logger_->info("MonitoringManager: Closing exit door");
    //close_exit_door_timer_->stop();
}

void MonitoringManager::FrontRgb(const std::string &data) {
    logger_->info("MonitoringManager: Received FrontRgb request.");
    //logger_->info("MonitoringManager: Data received => {}", data);
    QString Qdata = QString::fromStdString(data);
    QJsonObject object;
    QJsonDocument doc = QJsonDocument::fromJson(Qdata.toUtf8());
    if(!doc.isNull()){
        if(doc.isObject()){
            object = doc.object();
            if (object.contains("metadata")) {
                QJsonObject metadata = object.take("metadata").toObject();
                if (metadata.contains("r")){
                    int r = metadata.take("r").toInt();
                    io_manager_->WritePwm(OutputPins::kLightFacadeChannelR, r);
                    logger_->info("MonitoringManager: Writing {} to red", r);
                }
                if (metadata.contains("g")){
                    int g = metadata.take("g").toInt();
                    io_manager_->WritePwm(OutputPins::kLightFacadeChannelG, g);
                    logger_->info("MonitoringManager: Writing {} to green", g);
                }
                if (metadata.contains("b")){
                    int b = metadata.take("b").toInt();
                    io_manager_->WritePwm(OutputPins::kLightFacadeChannelB, b);
                    logger_->info("MonitoringManager: Writing {} to blue", b);
                }
            } else {
            logger_->info("MonitoringManager: error parsing JSON.");
            }
        }
    } else {
        logger_->info("MonitoringManager: invalid JSON.");
    }
}

void MonitoringManager::PostResponse(const QString &value,
                                     const PostIotEvents &event) {
    std::string metadata;
    QJsonObject join;
    QJsonObject response;
    logger_->info("Sending response to Iot Event: {}", static_cast<int>(event));
    try {
        metadata = kPostRoutes.at(event);
        if (metadata.empty()) {
            join.insert("store_iot_devices_type", static_cast<int>(event));
            response.insert("iot_event", join);
        } else {
            QJsonObject json_metadata {
                {QString::fromStdString(metadata), value}
            };
            join.insert("store_iot_devices_type", static_cast<int>(event));
            join.insert("metadata", json_metadata);
            response.insert("iot_event", join);
        }
        QJsonDocument doc(response);
        QString qdata(doc.toJson(QJsonDocument::Compact));
        logger_->info("Sending post response with data {}",
                      qdata.toStdString());
        rest_client_->PostRequest(qdata, "");
    } catch (std::out_of_range &ex) {
        logger_->error("Tried to access non valid iot event in post response."
                       "Iot Event: {}", static_cast<int>(event));
    }
}

VarStatus MonitoringManager::GetSnapshotVars(){
    snapshot_vars_.front_door_open = io_manager_->Read(OutputPins::kFrontDoor);
    snapshot_vars_.exit_door_open = io_manager_->Read(OutputPins::kExitDoor);
    snapshot_vars_.ligth_front_rgb = io_manager_->Read(OutputPins::kLightFacadePower);
    snapshot_vars_.ac_main_device = io_manager_->Read(OutputPins::kACDevice);
    snapshot_vars_.ac_aux_device = io_manager_->Read(OutputPins::kACDeviceAux);
    snapshot_vars_.store_open = open_store_;
    return snapshot_vars_;
}

}
}
