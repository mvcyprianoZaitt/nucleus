#ifndef DEBOUNCER_H
#define DEBOUNCER_H

#include <QTimer>

#include "src/IOManager.h"

namespace nucleus {
namespace monitoring {
class Debouncer : public QObject{
    Q_OBJECT
 public:
     Debouncer(std::shared_ptr<spdlog::logger> logger, std::shared_ptr<IOManager> io_manager);
     virtual ~Debouncer();
     void Execute();
 private:
     void CloseDoorCommand();
     std::shared_ptr<spdlog::logger> logger_;
     std::shared_ptr<IOManager> io_manager_;
     QTimer *close_door_timer_;
     int counter_;
};
}
}
#endif
