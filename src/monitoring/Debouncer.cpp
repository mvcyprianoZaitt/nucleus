#include "Debouncer.h"

#include <QTimer>

#include "src/IOManager.h"

namespace nucleus {
namespace monitoring {

Debouncer::Debouncer(std::shared_ptr<spdlog::logger> logger,
          std::shared_ptr<IOManager> io_manager)
        : logger_(logger),
          io_manager_(io_manager){
    close_door_timer_ = new QTimer(this);
    //close_door_timer_->setSingleShot(true);
    connect(close_door_timer_, &QTimer::timeout, this,
            &Debouncer::CloseDoorCommand);
    logger_->info("Debouncer: Initializing Debouncer for the exit button.");
    io_manager_->Write(OutputPins::kExitDoor, false);
}


Debouncer::~Debouncer(){

}


void Debouncer::Execute(){
    if (io_manager_->Read(OutputPins::kExitDoor))
        return;
    if (io_manager_->Read(InputPins::kExitDoorEmergency))
        counter_ = 0;
    else
        counter_++;
    if (counter_ >= 2){
        int time_ms = 3000;
        io_manager_->Write(OutputPins::kExitDoor, true);
        logger_->info("Debouncer: Open door requisition");
        if (close_door_timer_->isActive()) {
            close_door_timer_->stop();
        }
        close_door_timer_->start(time_ms);
    }
}

void Debouncer::CloseDoorCommand() {
    io_manager_->Write(OutputPins::kExitDoor, false);
    logger_->info("Debouncer: Close door requisition");
    close_door_timer_->stop();
}

}
}
