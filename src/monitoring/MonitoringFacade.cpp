#include "MonitoringFacade.h"

#include <QObject>

#include <memory>

#include "MonitoringManager.h"
#include "SnapshotManager.h"
#include "spdlog/spdlog.h"
#include "src/IOManager.h"
#include "src/iot_client/PusherClient.h"
#include "src/dbus/RestClientApi.h"

using nucleus::iot_client::PusherClient;
using nucleus::dbus::RestClientApi;

namespace nucleus {
namespace monitoring {
MonitoringFacade::MonitoringFacade(std::shared_ptr<spdlog::logger> logger,
        std::shared_ptr<IOManager> io_manager,
        std::shared_ptr<PusherClient> pusher_client, VarStatus snapshot_vars)
        : rest_client_(std::make_shared<RestClientApi>(logger)),
          monitoring_manager_(std::make_shared<MonitoringManager>(logger,
                        pusher_client, io_manager, rest_client_, snapshot_vars)),
          snapshot_manager_(std::make_shared<SnapshotManager> (logger, monitoring_manager_)) {

}

std::shared_ptr<SnapshotManager> MonitoringFacade::GetSnapshotManager(){
    return snapshot_manager_;
}

}
}
