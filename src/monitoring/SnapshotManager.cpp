#include "SnapshotManager.h"

#include <QDate>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>

#include "MonitoringManager.h"
#include "src/defines.h"
#include "spdlog/spdlog.h"

namespace nucleus {
namespace monitoring {
SnapshotManager::SnapshotManager(std::shared_ptr<spdlog::logger> logger,
                      std::shared_ptr<MonitoringManager> monitoring_manager)
        : logger_(logger),
          file_name_(SNAPSHOT_FILE),
          monitoring_manager_(monitoring_manager),
          snapshot_exec_counter_(0){
    logger_->info("SnapshotManager: Starting SnapshotManager.");
}

SnapshotManager::~SnapshotManager(){

}

void SnapshotManager::UpdateJson(){
    logger_->info("SnapshotManager: Updating Json file.");
    VarStatus current_status = monitoring_manager_->GetSnapshotVars();
    QString today = QDate::currentDate().toString("dd.MM.yyyy");
    QString now = QTime::currentTime().toString("hh:mm:ss");

    QJsonObject js_time_object;
    js_time_object.insert("date", today);
    js_time_object.insert("time", now);

    QJsonObject js_internal_vars_object;
    js_internal_vars_object.insert("front_door_open",
                                    current_status.front_door_open);
    js_internal_vars_object.insert("exit_door_open",
                                    current_status.exit_door_open);
    js_internal_vars_object.insert("ligth_front_rgb",
                                    current_status.ligth_front_rgb);
    js_internal_vars_object.insert("ac_main_device",
                                    current_status.ac_main_device);
    js_internal_vars_object.insert("ac_aux_device",
                                    current_status.ac_aux_device);
    js_internal_vars_object.insert("store_open", current_status.store_open);

    json_object_.insert("last_snapshot", js_time_object);
    json_object_.insert("internal_state", js_internal_vars_object);
}

void SnapshotManager::Execute(){
    snapshot_exec_counter_++;
    if (snapshot_exec_counter_ >= 600){
        UpdateJson();
        QJsonDocument json_document(json_object_);
        QFile jsonFile(file_name_);
        jsonFile.open(QFile::WriteOnly);
        jsonFile.write(json_document.toJson());
        logger_->info("SnapshotManager: JSon file successfully written as {}.",
                file_name_.toStdString());
        snapshot_exec_counter_ = 0;
    }
}

}
}
