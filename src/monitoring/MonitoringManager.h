#ifndef MONITORINGMANAGER_H
#define MONITORINGMANAGER_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QTimer>
#include <QString>

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <utility>

#include "spdlog/spdlog.h"
#include "src/dbus/AudioClient.h"
#include "src/iot_client/PusherClient.h"
#include "src/dbus/RestClientApi.h"
#include "src/IOManager.h"
#include "Temperature.h"

namespace nucleus {
namespace monitoring {
enum class PostIotEvents {
    kDoorTimeout = 9,
    kSmokeDetector = 17,
    kTempResponse = 19,
    kACDeviceAux = 20,
    kLightCircuitStatus = 21,
};
class MonitoringManager : public QObject {
    Q_OBJECT

 public:
     MonitoringManager(std::shared_ptr<spdlog::logger> logger,
            std::shared_ptr<nucleus::iot_client::PusherClient> pusher_client,
            std::shared_ptr<IOManager> io_manager,
            std::shared_ptr<nucleus::dbus::RestClientApi> rest_client,
            VarStatus snapshot_vars);
     ~MonitoringManager();
      void SubscribeCallbacks();
      VarStatus GetSnapshotVars();

 private:
     static const std::map<PostIotEvents, std::string> kPostRoutes;
     void TemperatureRequestEvent(const std::string &data);
     void OpenStoreRequestEvent(const std::string &data);
     void CloseStoreRequestEvent(const std::string &data);
     void OpenDoorPermanentlyRequestEvent(const std::string &data);
     void CloseDoorRequestEvent(const std::string &data);
     void OpenDoorMomentRequestEvent(const std::string &data);
     void AudioRequestEvent(const std::string &data);
     void OpenDoorClientRequestEvent(const std::string &data);
     void OpenEntranceDoorClientRequestEvent(const std::string &data);
     void OpenExitDoorClientRequestEvent(const std::string &data);
     void SendACStatusRequestEvent(const std::string &data);
     void TurnOnACRequestEvent(const std::string &data);
     void TurnOffACRequestEvent(const std::string &data);
     void SendLuminosityStatusRequestEvent(const std::string &data);
     void TurnOnLightsRequestEvent(const std::string &data);
     void TurnOffLightsRequestEvent(const std::string &data);
     void CloseDoorCommand();
     void CloseEntranceDoorCommand();
     void CloseExitDoorCommand();
     void CheckDoor();
     void FrontRgb(const std::string &data);
     void PostResponse(const QString &value, const PostIotEvents &event);
     std::shared_ptr<spdlog::logger> logger_;
     std::shared_ptr<nucleus::iot_client::PusherClient> pusher_client_;
     std::shared_ptr<IOManager> io_manager_;
     std::shared_ptr<nucleus::dbus::RestClientApi> rest_client_;
     bool open_store_;
     bool persist_door_open_;
     int entrance_door_count_;
     int exit_door_count_;
     QTimer *close_door_timer_;
     QTimer *close_entrance_door_timer_;
     QTimer *close_exit_door_timer_;
     QTimer *check_door_timer_;
     Temperature temperature_;
     VarStatus snapshot_vars_;
     nucleus::dbus::AudioClient audio_;
};

}
}

#endif
